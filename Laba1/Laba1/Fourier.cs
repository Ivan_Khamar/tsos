﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Laba1
{
    public class Fourier
    {
        ///////////////////////////////////////////////////////////////////////////////////////////
        //оголошення полів і констант для ряду
        ///////////////////////////////////////////////////////////////////////////////////////////
        private readonly int n;
        private const int iterationNumber = 1000;
        private readonly double from;
        private readonly double to;
        private List<double> lastACoeficients = new List<double>();
        private List<double> lastBCoeficients = new List<double>();

        ///////////////////////////////////////////////////////////////////////////////////////////
        //конструктор для об'єкта ряду
        ///////////////////////////////////////////////////////////////////////////////////////////

        public Fourier(int givenVariantNumber, double givenFrom, double givenTo)
        {
            n = givenVariantNumber;
            from = givenFrom;
            to = givenTo;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////
        //метод для точного аналітичного обчислення. Пункт 1. Номер в списку 7 -> друга формула
        ///////////////////////////////////////////////////////////////////////////////////////////
       
        public double FunctionCalculation(double x)
        {
            if (x >= from && x <= to)
                return n * Math.Sin(Math.PI * n * x);
            else
                throw new ArgumentException("enter valid x please (x should be in propper limits)");
        }

        ///////////////////////////////////////////////////////////////////////////////////////////
        //методи для обчислення коефііцієнтів. Пункт 2. 
        ///////////////////////////////////////////////////////////////////////////////////////////

        private double ACoeficientFunction(double x, int k)
        {
            return FunctionCalculation(x) * Math.Cos(k * x);

        }
        private double BCoeficientFunction(double x, int k)
        {
            return FunctionCalculation(x) * Math.Sin(k * x);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////
        //методи для обчислення наближення. Пункт 3. 
        ///////////////////////////////////////////////////////////////////////////////////////////

        private delegate double CoeficientFunction(double x, int k);

        private double IntegrationOfFunction(int k, CoeficientFunction coeficientFunction)
        {
            double x, dist, sum = 0, sumT = 0;
            dist = (to - from) / iterationNumber;
            for (int i = 1; i <= iterationNumber; i++)
            {
                x = from + i * dist;
                sumT += coeficientFunction(x, k);
                if (i < iterationNumber)
                {
                    sum += coeficientFunction(x, k);
                }
            }
            sum = (dist / 6) *
                (coeficientFunction(from, k) + coeficientFunction(to, k) + 2 * sum + 4 * sumT);

            return sum;
        }

        private double ACoeficientCalculation(int k)
        {
            return 1 / Math.PI * IntegrationOfFunction(k, ACoeficientFunction);
        }

        private double BCoeficientCalculation(int k)
        {
            return 1 / Math.PI * IntegrationOfFunction(k, BCoeficientFunction);
        }

        public double FourierCalculation(double x, int N)
        {
            double suma = 0;
            double a = 0;
            double b = 0;
            lastACoeficients.Clear();
            lastBCoeficients.Clear();

            suma = ACoeficientCalculation(0) / 2;
            for (int k = 1; k <= N; k++)
            {
                a = ACoeficientCalculation(k);
                lastACoeficients.Add(a);
                suma += a * Math.Cos(k * x);
                b = BCoeficientCalculation(k);
                lastBCoeficients.Add(a);
                suma += b * Math.Sin(k * x);
            }
            return suma;
        }

        public List<double> GetLastACoeficients()
        {
            return lastACoeficients;
        }

        public List<double> GetLastBCoeficients()
        {
            return lastBCoeficients;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////
        //метод для обчислення відносної похибки. Пункт 4. 
        ///////////////////////////////////////////////////////////////////////////////////////////

        public double RelativeDifference(double expected, double actual)
        {
            return Math.Abs((actual - expected) / expected);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////
        //метод для зберігання в файл отриманих результатів. Пункт 5. 
        ///////////////////////////////////////////////////////////////////////////////////////////

        public void FilePrintResults(string filename, int N, List<double> ACoeficients,
            List<double> BCoeficients, double relativeDifference)
        {
            string formatedResult;

            formatedResult = $"{DateTime.Now}\nN: {N}\na coefitients:\n";
            for (int i = 0; i < ACoeficients.Count; i++)
            {
                formatedResult += $"\t{i + 1}. {ACoeficients[i]}\n";
            }

            formatedResult += "\nb coefitients:\n";
            for (int i = 0; i < BCoeficients.Count; i++)
            {
                formatedResult += $"\t{i + 1}. {BCoeficients[i]}\n";
            }

            formatedResult += $"Relative difference: {relativeDifference * 100}%\n\n\n";

            File.AppendAllText(filename, formatedResult);
        }
    }

}