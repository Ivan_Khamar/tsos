﻿using System;

namespace Laba1
{
    class MainClass
    {

        ///////////////////////////////////////////////////////////////////////////////////////////
        //головна програма. Пункт 6. 
        ///////////////////////////////////////////////////////////////////////////////////////////

        static void Main(string[] args)
        {
            double relativeDiference = 0;
            Fourier fourier = new Fourier(7, 0, Math.PI);

            Console.WriteLine("Enter x: ");
            double x = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter N");
            int N = int.Parse(Console.ReadLine());

            try
            {
                double actualFunction = fourier.FunctionCalculation(x);
                Console.WriteLine("Actual function value: " + actualFunction);
                double fourierAproximateValue = fourier.FourierCalculation(x, N);
                Console.WriteLine("Fourier aproximate value: " + fourierAproximateValue);
                relativeDiference = fourier.RelativeDifference(actualFunction, fourierAproximateValue);
                Console.WriteLine($"Relative degree difference: {relativeDiference * 100}%");
            }
            catch (Exception excpetion)
            {
                Console.WriteLine(excpetion.ToString());
            }

            fourier.FilePrintResults("results.txt", N,
                fourier.GetLastACoeficients(), fourier.GetLastBCoeficients(), relativeDiference);

            Console.ReadKey();
        }
    }
}

