﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace Lab_2
{
    public partial class Form1 : Form
    {
        const float A = 3;
        const float fi = (float) Math.PI / 3; 
        List<float> x;
        List<float> y;
        List<float> yDeviated;
        List<float> SMA;
        List<float> EMA;
        List<float> WMA;
        List<float> MMA;

        public Form1()
        {
            InitializeComponent();
            GetData();
            CalculateApprox();
            DrawGR();
        }

        public void GetData()
        {
            Random random = new Random();
            x = new List<float>();
            yDeviated = new List<float>();
            y = new List<float>();
            for (float x = 0; x <= 10.01; x = x + 0.01f)
            {
                this.x.Add(x);
                y.Add((float)(A * Math.Sin(x + fi)));
                yDeviated.Add((float)(A * Math.Sin(x + fi)) * (float) (random.NextDouble() % 0.05 + 1));
            }
        }

        public void CalculateApprox()
        {
            SMA = Functions.SMA(yDeviated, (int)numericUpDown1.Value);
            EMA = Functions.EMA(yDeviated, (int)numericUpDown1.Value);
            WMA = Functions.WMA(yDeviated, (int)numericUpDown1.Value);
            MMA = Functions.MMA(yDeviated, (int)numericUpDown1.Value);
        }

        public void DrawGR()
        {
            Bitmap b = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            Graphics g = Graphics.FromImage(b);
            g.TranslateTransform(0,b.Height / 2);
            g.DrawLine(new Pen(Color.Silver), 0, 0, b.Width, 0);
            g.DrawLine(new Pen(Color.Silver), 0, b.Height/2, 0, -b.Height / 2);
            var dy = (float)b.Height / 14;
            var dx = (float)b.Width / 10;
            for(int i = 1; i < x.Count; i++)
            {
                if (checkBox1.Checked)
                {
                    g.DrawLine(new Pen(Color.Red), x[i] * dx, -y[i] * dy, x[i - 1] * dx, -y[i - 1] * dy);
                }
                if(checkBox2.Checked)
                {
                    g.DrawLine(new Pen(Color.Blue), x[i] * dx, -yDeviated[i] * dy, x[i - 1] * dx, -yDeviated[i - 1] * dy);
                }
                if (checkBox3.Checked)
                {
                    var tmp = SMA;
                    g.DrawLine(new Pen(Color.Green), x[i] * dx, -tmp[i] * dy, x[i - 1] * dx, -tmp[i - 1] * dy);
                    label1.Text = Functions.getString(tmp, y);
                }
                else
                {
                    label1.Text = "";
                }
                if (checkBox4.Checked)
                {
                    var tmp = EMA;
                    g.DrawLine(new Pen(Color.Orange), x[i] * dx, -tmp[i] * dy, x[i - 1] * dx, -tmp[i - 1] * dy);
                    label2.Text = Functions.getString(tmp, y);
                }
                else
                {
                    label2.Text = "";
                }
                if (checkBox5.Checked)
                {
                    var tmp = WMA;
                    g.DrawLine(new Pen(Color.Tomato), x[i] * dx, -tmp[i] * dy, x[i - 1] * dx, -tmp[i - 1] * dy);
                    label3.Text = Functions.getString(tmp, y);
                }
                else
                {
                    label3.Text = "";
                }
                if (checkBox6.Checked)
                {
                    var tmp = MMA;
                    g.DrawLine(new Pen(Color.YellowGreen), x[i] * dx, -tmp[i] * dy, x[i - 1] * dx, -tmp[i - 1] * dy);
                    label4.Text = Functions.getString(tmp, y);
                }
                else
                {
                    label4.Text = "";
                }
            }
            pictureBox1.BackgroundImage = b;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            var dx = (float)pictureBox1.Width / 10;
            int index = (int)(e.X / dx / 0.01);
            if (checkBox3.Checked) label6.Text = SMA[index].ToString("0.00");
            if (checkBox4.Checked) label7.Text = EMA[index].ToString("0.00");
            if (checkBox5.Checked) label8.Text = WMA[index].ToString("0.00");
            if (checkBox6.Checked) label9.Text = MMA[index].ToString("0.00");
            if (checkBox1.Checked) label10.Text = y[index].ToString("0.00");
            if (checkBox2.Checked) label11.Text = yDeviated[index].ToString("0.00");
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            DrawGR();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CalculateApprox();
            DrawGR();
        }
    }
}
