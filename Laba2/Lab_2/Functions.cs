﻿using System;
using System.Collections.Generic;

namespace Lab_2
{
    class Functions
    {
        public static List<float> SMA(List<float> y, int n)
        {
            List<float> new_y = new List<float>();
            for(int i = 0; i < n-1; i++)
            {
                float sum = 0.0f;
                for (int j = 0; j <= i; j++)
                {
                    sum += y[j];
                }
                new_y.Add(sum / (i + 1));
            }
            for (int i = n-1; i < y.Count; i++)
            {
                float sum = 0.0f;
                for(int j = 0; j < n; j++)
                {
                    sum += y[i-j];
                }
                new_y.Add(sum / n);
            }
            return new_y;
        }
        public static List<float> EMA(List<float> y, int n)
        {
            List<float> new_y = new List<float>();
            for (int i = 0; i < n; i++)
            {
                float sum = 0.0f;
                for (int j = 0; j <= i; j++)
                {
                    sum += y[j];
                }
                new_y.Add(sum / (i + 1));
            }
            float alpha = 2.0f / (n + 1);
            for (int i = n; i < y.Count; i++)
            {
                new_y.Add(y[i]*alpha+new_y[i-1]*(1-alpha));
            }
            return new_y;
        }
        public static List<float> WMA(List<float> y, int n)
        {
            List<float> new_y = new List<float>();
            new_y.Add(y[0]);
            float zn = 0.0f;
            for (int i = 2; i < n; i++)
            {
                zn = 0.0f;
                float ch = 0.0f;
                for(int j = 0; j < i; j++)
                {
                    zn += (i - j);
                    ch += y[i - j - 1] * (i - j);
                }
                new_y.Add(ch / zn);
            }
            zn = 0.0f;
            for(int i = 0; i < n; i++)
            {
                zn += i + 1;
            }
            for(int i = n-1; i < y.Count; i++)
            {
                float ch = 0.0f;
                for(int j = 0; j < n; j++)
                {
                    ch += y[i - j] * (n - j);
                }
                new_y.Add(ch / zn);
            }
            return new_y;
        }
        public static List<float> MMA(List<float> y, int n)
        {
            List<float> new_y = new List<float>();
            new_y.Add(y[0]);
            for(int i = 1; i < n; i++)
            {
                new_y.Add((y[i] + i * new_y[i - 1]) / (i + 1));
            }
            for(int i = n; i < y.Count; i++)
            {
                new_y.Add((y[i]+(n-1)*new_y[i-1])/(n));
            }
            return new_y;
        }
        public static float[] getMinMax(List<float> actual, List<float> expected)
        {
            float absoluteMax = Math.Abs(actual[1] - expected[1]);
            float relativeMax = absoluteMax/ expected[1];
            float absoluteMin = absoluteMax;
            float relativeMin = relativeMax;

            for(int i = 2; i < actual.Count; i++)
            {
                var x = Math.Abs(actual[i] - expected[i]);
                if (x < absoluteMin)
                {
                    absoluteMin = x;
                    relativeMin = x / expected[i];
                }
                if (x > absoluteMax)
                {
                    absoluteMax = x;
                    relativeMax = x / expected[i];
                }
            }

            float[] res = new float[4];
            res[0] = Math.Abs(absoluteMin);
            res[1] = Math.Abs(relativeMin);
            res[2] = Math.Abs(absoluteMax);
            res[3] = Math.Abs(relativeMax);
            return res;
        }

        public static float average_arithmetic(List<float> y, int start_i, int end_i)
        {
            float sum = 0;
            for(int i = start_i; i <= end_i; i++)
            {
                sum += y[i];
            }
            return sum / (end_i - start_i + 1);
        }

        public static double average_geo(List<float> y, int start_i, int end_i)
        {
            float d = 1;
            for (int i = start_i; i <= end_i; i++)
            {
                d *= y[i];
            }
            return Math.Pow(d, (1.0/(end_i - start_i + 1)));
        }

        public static double average_gar(List<float> y, int start_i, int end_i)
        {
            var sum = 0.0f;
            for (int i = start_i; i <= end_i; i++)
            {
                sum += 1.0f/y[i];
            }
            return (end_i - start_i + 1)/sum;
        }

        public static String getString(List<float> y1, List<float> y_real)
        {
            var t = getMinMax(y1, y_real);
            String s = $"min = {t[1].ToString("0.00")}%, max = {t[3].ToString("0.00")}%";
            return s;
        }
    }
}
