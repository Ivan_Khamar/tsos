﻿namespace Laba3
{
    public static class Fourier
    {
        #region enums
        public enum Direction
        {
            Forward = 1,
            Backward = -1
        };

        #endregion enums

        #region calculation for DFT

        public static calculationMethodsStructure[] DFT(calculationMethodsStructure[] data, Direction direction)
        {
            int n = data.Length;
            double arg, cos, sin;
            calculationMethodsStructure[] dst = new calculationMethodsStructure[n];

            // for each destination element
            for (int i = 0; i < n; i++)
            {
                dst[i] = calculationMethodsStructure.DoubleNull;

                arg = -(int)direction * 2.0 * System.Math.PI * (double)i / (double)n;

                // sum source elements
                for (int j = 0; j < n; j++)
                {
                    cos = System.Math.Cos(j * arg);
                    sin = System.Math.Sin(j * arg);

                    dst[i].RealPart += (data[j].RealPart * cos - data[j].ImaginePart * sin);
                    dst[i].ImaginePart += (data[j].RealPart * sin + data[j].ImaginePart * cos);
                }
            }

            // copy elements
            if (direction == Direction.Backward)
            {
                // devide also for forward transform
                for (int i = 0; i < n; i++)
                {
                    data[i].RealPart = dst[i].RealPart / n;
                    data[i].ImaginePart = dst[i].ImaginePart / n;
                }
            }
            else
            {
                for (int i = 0; i < n; i++)
                {
                    data[i].RealPart = dst[i].RealPart;
                    data[i].ImaginePart = dst[i].ImaginePart;
                }
            }

            return data;
        }
        #endregion calculation for DFT
    }
}
