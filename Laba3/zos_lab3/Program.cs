﻿using Laba3;
using System;
using System.IO;

namespace Calc
{
    class Calc
    {
        #region main function
        static void Main(string[] args)
        {
            int N = 1000;
            var st = new double[N];
            while (true)
            {
                StreamWriter input = new StreamWriter("input.txt");

                for (int i = 0; i < st.Length; i++)
                {
                    st[i] = Math.Cos(Math.PI * 1 / N * i);
                    input.WriteLine("var" + i + " = " + st[i]);
                }

                input.Close();

                Console.WriteLine(
                    "Please enter command:\n FRONT - For DFT to front, \n BACK - For DFT to back, \n QUIT - For quit program\n");

                string value = Console.ReadLine();

                if (value == "FRONT")
                {
                    Console.Write("Making DFT, please wait\n");

                    var watch = System.Diagnostics.Stopwatch.StartNew();
                    var d = PreTransform(st);
                    double normalizer = 1.0 / st.Length * 2;
                    calculationMethodsStructure[] inputSignal = new calculationMethodsStructure[N];
                    for (int i = 0; i < d.Item1.Length; i++)
                    {
                        calculationMethodsStructure temp = new calculationMethodsStructure(d.Item1[i] * normalizer, d.Item2[i] * normalizer);
                        inputSignal[i] = temp;
                    }

                    var res = Fourier.DFT(inputSignal, Fourier.Direction.Forward);
                    watch.Stop();
                    var elapsedMs = watch.ElapsedMilliseconds;

                    StreamWriter complexNumberStream = new StreamWriter("output.txt");

                    for (int i = 0; i < res.Length; i++)
                    {
                        complexNumberStream.WriteLine("real" + i + " = " + res[i].RealPart);
                        complexNumberStream.WriteLine("imag" + i + " = " + res[i].ImaginePart);
                    }

                    complexNumberStream.Close();

                    Console.Write("Number of operations [O(N^2)]: " + N * N + "\n");
                    Console.Write("Forward DFT execution time: " + elapsedMs +
                                  " ms \nForward DFT ended successfully. Check txt files.\n\n\n");

                }
                else if (value == "BACK")
                {
                    Console.Write("Making backward DFT, please wait\n");

                    var watch = System.Diagnostics.Stopwatch.StartNew();
                    var d = PreTransform(st);
                    double normalizer = 1.0 / st.Length * 2;
                    calculationMethodsStructure[] inputSignal = new calculationMethodsStructure[N];
                    for (int i = 0; i < d.Item1.Length; i++)
                    {
                        calculationMethodsStructure temp = new calculationMethodsStructure(d.Item1[i] * normalizer, d.Item2[i] * normalizer);
                        inputSignal[i] = temp;
                    }

                    var res = Fourier.DFT(inputSignal, Fourier.Direction.Backward);
                    watch.Stop();
                    var elapsedMs = watch.ElapsedMilliseconds;

                    StreamWriter complexNumberStream = new StreamWriter("output.txt");

                    for (int i = 0; i < res.Length; i++)
                    {
                        complexNumberStream.WriteLine("real" + i + " = " + res[i].RealPart);
                        complexNumberStream.WriteLine("imag" + i + " = " + res[i].ImaginePart);
                    }

                    complexNumberStream.Close();

                    Console.Write("Backward DFT execution time: " + elapsedMs +
                                  " ms \nBackward DFT ended successfully. Check txt files.\n\n\n");

                }
                else if (value == "QUIT")
                {
                    System.Environment.Exit(1);
                }
                else
                {
                    Console.WriteLine("Please, enter command from a list.\n");
                }
            }
        }

        #endregion main function

        #region signal transformation
        // valid input signal transformation
        public static Tuple<double[], double[]> PreTransform(double[] input, int partials = 0)
        {
            int len = input.Length;
            double[] cosDFT = new double[len];
            double[] sinDFT = new double[len];

            if (partials == 0)
                partials = len / 2;

            for (int n = 0; n <= partials; n++)
            {
                double cos = 0.0;
                double sin = 0.0;

                for (int i = 0; i < len; i++)
                {
                    cos += input[i] * Math.Cos(2 * Math.PI * n / len * i);
                    sin += input[i] * Math.Sin(2 * Math.PI * n / len * i);
                }

                cosDFT[n] = cos;
                sinDFT[n] = sin;
            }

            return new Tuple<double[], double[]>(cosDFT, sinDFT);
        }
        #endregion signal transformation
    }
}