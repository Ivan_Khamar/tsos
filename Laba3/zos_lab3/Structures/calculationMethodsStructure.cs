﻿namespace Laba3
{
    using System;
    using System.Text.RegularExpressions;
    using System.Runtime.Serialization;

    public struct calculationMethodsStructure : ICloneable, ISerializable
    {
        #region init
        public double RealPart;
        public double ImaginePart;
        public static readonly calculationMethodsStructure DoubleNull = new calculationMethodsStructure(0, 0);
        public static readonly calculationMethodsStructure DoubleOne = new calculationMethodsStructure(1, 0);
        public static readonly calculationMethodsStructure DoubleRootOfMinusOne = new calculationMethodsStructure(0, 1);
        #endregion

        #region getters
        public double Magnitude
        {
            get { return System.Math.Sqrt(RealPart * RealPart + ImaginePart * ImaginePart); }
        }

        public double Phase
        {
            get { return System.Math.Atan2(ImaginePart, RealPart); }
        }

        public double SquaredMagnitude
        {
            get { return (RealPart * RealPart + ImaginePart * ImaginePart); }
        }

        #endregion

        #region constructors
        public calculationMethodsStructure(double re, double im)
        {
            this.RealPart = re;
            this.ImaginePart = im;
        }

        public calculationMethodsStructure(calculationMethodsStructure c)
        {
            this.RealPart = c.RealPart;
            this.ImaginePart = c.ImaginePart;
        }

        #endregion constructors

        #region static constructors
        public static calculationMethodsStructure Add(calculationMethodsStructure a, calculationMethodsStructure b)
        {
            return new calculationMethodsStructure(a.RealPart + b.RealPart, a.ImaginePart + b.ImaginePart);
        }

        public static calculationMethodsStructure Add(calculationMethodsStructure a, double s)
        {
            return new calculationMethodsStructure(a.RealPart + s, a.ImaginePart);
        }
        #endregion static constructors

        #region adding
        public static void Add(calculationMethodsStructure a, calculationMethodsStructure b, ref calculationMethodsStructure result)
        {
            result.RealPart = a.RealPart + b.RealPart;
            result.ImaginePart = a.ImaginePart + b.ImaginePart;
        }

        public static void Add(calculationMethodsStructure a, double s, ref calculationMethodsStructure result)
        {
            result.RealPart = a.RealPart + s;
            result.ImaginePart = a.ImaginePart;
        }

        #endregion adding

        #region substraction
        public static calculationMethodsStructure Subtract(calculationMethodsStructure a, calculationMethodsStructure b)
        {
            return new calculationMethodsStructure(a.RealPart - b.RealPart, a.ImaginePart - b.ImaginePart);
        }

        public static calculationMethodsStructure Subtract(calculationMethodsStructure a, double s)
        {
            return new calculationMethodsStructure(a.RealPart - s, a.ImaginePart);
        }

        public static calculationMethodsStructure Subtract(double s, calculationMethodsStructure a)
        {
            return new calculationMethodsStructure(s - a.RealPart, a.ImaginePart);
        }

        public static void Subtract(calculationMethodsStructure a, calculationMethodsStructure b, ref calculationMethodsStructure result)
        {
            result.RealPart = a.RealPart - b.RealPart;
            result.ImaginePart = a.ImaginePart - b.ImaginePart;
        }

        public static void Subtract(calculationMethodsStructure a, double s, ref calculationMethodsStructure result)
        {
            result.RealPart = a.RealPart - s;
            result.ImaginePart = a.ImaginePart;
        }

        public static void Subtract(double s, calculationMethodsStructure a, ref calculationMethodsStructure result)
        {
            result.RealPart = s - a.RealPart;
            result.ImaginePart = a.ImaginePart;
        }

        #endregion substraction

        #region multiplication
        public static calculationMethodsStructure Multiply(calculationMethodsStructure a, calculationMethodsStructure b)
        {
            // (x + yi)(u + vi) = (xu  yv) + (xv + yu)i. 
            double aRe = a.RealPart, aIm = a.ImaginePart;
            double bRe = b.RealPart, bIm = b.ImaginePart;

            return new calculationMethodsStructure(aRe * bRe - aIm * bIm, aRe * bIm + aIm * bRe);
        }

        public static calculationMethodsStructure Multiply(calculationMethodsStructure a, double s)
        {
            return new calculationMethodsStructure(a.RealPart * s, a.ImaginePart * s);
        }

        public static void Multiply(calculationMethodsStructure a, calculationMethodsStructure b, ref calculationMethodsStructure result)
        {
            // (x + yi)(u + vi) = (xu  yv) + (xv + yu)i. 
            double aRe = a.RealPart, aIm = a.ImaginePart;
            double bRe = b.RealPart, bIm = b.ImaginePart;

            result.RealPart = aRe * bRe - aIm * bIm;
            result.ImaginePart = aRe * bIm + aIm * bRe;
        }

        public static void Multiply(calculationMethodsStructure a, double s, ref calculationMethodsStructure result)
        {
            result.RealPart = a.RealPart * s;
            result.ImaginePart = a.ImaginePart * s;
        }

        #endregion multiplication

        #region division
        public static calculationMethodsStructure Divide(calculationMethodsStructure a, calculationMethodsStructure b)
        {
            double aRe = a.RealPart, aIm = a.ImaginePart;
            double bRe = b.RealPart, bIm = b.ImaginePart;
            double modulusSquared = bRe * bRe + bIm * bIm;

            if (modulusSquared == 0)
            {
                throw new DivideByZeroException("Can not divide by zero.");
            }

            double invModulusSquared = 1 / modulusSquared;

            return new calculationMethodsStructure(
                (aRe * bRe + aIm * bIm) * invModulusSquared,
                (aIm * bRe - aRe * bIm) * invModulusSquared);
        }

        public static calculationMethodsStructure Divide(calculationMethodsStructure a, double s)
        {
            if (s == 0)
            {
                throw new DivideByZeroException("Can not divide by zero.");
            }

            return new calculationMethodsStructure(a.RealPart / s, a.ImaginePart / s);
        }

        public static calculationMethodsStructure Divide(double s, calculationMethodsStructure a)
        {
            if ((a.RealPart == 0) || (a.ImaginePart == 0))
            {
                throw new DivideByZeroException("Can not divide by zero.");
            }
            return new calculationMethodsStructure(s / a.RealPart, s / a.ImaginePart);
        }

        public static void Divide(calculationMethodsStructure a, calculationMethodsStructure b, ref calculationMethodsStructure result)
        {
            double aRe = a.RealPart, aIm = a.ImaginePart;
            double bRe = b.RealPart, bIm = b.ImaginePart;
            double modulusSquared = bRe * bRe + bIm * bIm;

            if (modulusSquared == 0)
            {
                throw new DivideByZeroException("Can not divide by zero.");
            }

            double invModulusSquared = 1 / modulusSquared;

            result.RealPart = (aRe * bRe + aIm * bIm) * invModulusSquared;
            result.ImaginePart = (aIm * bRe - aRe * bIm) * invModulusSquared;
        }

        public static void Divide(calculationMethodsStructure a, double s, ref calculationMethodsStructure result)
        {
            if (s == 0)
            {
                throw new DivideByZeroException("Can not divide by zero.");
            }

            result.RealPart = a.RealPart / s;
            result.ImaginePart = a.ImaginePart / s;
        }

        public static void Divide(double s, calculationMethodsStructure a, ref calculationMethodsStructure result)
        {
            if ((a.RealPart == 0) || (a.ImaginePart == 0))
            {
                throw new DivideByZeroException("Can not divide by zero.");
            }

            result.RealPart = s / a.RealPart;
            result.ImaginePart = s / a.ImaginePart;
        }

        #endregion division

        #region negation
        public static calculationMethodsStructure Negate(calculationMethodsStructure a)
        {
            return new calculationMethodsStructure(-a.RealPart, -a.ImaginePart);
        }

        #endregion negation

        #region approximation
        public static bool ApproxEqual(calculationMethodsStructure a, calculationMethodsStructure b)
        {
            return ApproxEqual(a, b, 8.8817841970012523233891E-16);
        }

        public static bool ApproxEqual(calculationMethodsStructure a, calculationMethodsStructure b, double tolerance)
        {
            return
                (
                (System.Math.Abs(a.RealPart - b.RealPart) <= tolerance) &&
                (System.Math.Abs(a.ImaginePart - b.ImaginePart) <= tolerance)
                );
        }

        #endregion approximation

        #region Public Static Parse Methods
        public static calculationMethodsStructure Parse(string s)
        {
            Regex r = new Regex(@"\((?<real>.*),(?<imaginary>.*)\)", RegexOptions.None);
            Match m = r.Match(s);

            if (m.Success)
            {
                return new calculationMethodsStructure(
                    double.Parse(m.Result("${real}")),
                    double.Parse(m.Result("${imaginary}"))
                    );
            }
            else
            {
                throw new FormatException("String representation of the complex number is not correctly formatted.");
            }
        }
        
        public static bool TryParse(string s, out calculationMethodsStructure result)
        {
            try
            {
                calculationMethodsStructure newComplex = calculationMethodsStructure.Parse(s);
                result = newComplex;
                return true;

            }
            catch (FormatException)
            {

                result = new calculationMethodsStructure();
                return false;
            }
        }

        #endregion

        #region Public Static Complex Special Functions

        public static calculationMethodsStructure Sqrt(calculationMethodsStructure a)
        {
            calculationMethodsStructure result = calculationMethodsStructure.DoubleNull;

            if ((a.RealPart == 0.0) && (a.ImaginePart == 0.0))
            {
                return result;
            }
            else if (a.ImaginePart == 0.0)
            {
                result.RealPart = (a.RealPart > 0) ? System.Math.Sqrt(a.RealPart) : System.Math.Sqrt(-a.RealPart);
                result.ImaginePart = 0.0;
            }
            else
            {
                double modulus = a.Magnitude;

                result.RealPart = System.Math.Sqrt(0.5 * (modulus + a.RealPart));
                result.ImaginePart = System.Math.Sqrt(0.5 * (modulus - a.RealPart));
                if (a.ImaginePart < 0.0)
                    result.ImaginePart = -result.ImaginePart;
            }

            return result;
        }

        /// <summary>
        /// Calculates natural (base <b>e</b>) logarithm of a complex number.
        /// </summary>
        /// 
        /// <param name="a">A <see cref="calculationMethodsStructure"/> instance.</param>
        /// 
        /// <returns>Returns new <see cref="calculationMethodsStructure"/> instance containing the natural logarithm of the specified
        /// complex number.</returns>
        /// 
        public static calculationMethodsStructure Log(calculationMethodsStructure a)
        {
            calculationMethodsStructure result = calculationMethodsStructure.DoubleNull;

            if ((a.RealPart > 0.0) && (a.ImaginePart == 0.0))
            {
                result.RealPart = System.Math.Log(a.RealPart);
                result.ImaginePart = 0.0;
            }
            else if (a.RealPart == 0.0)
            {
                if (a.ImaginePart > 0.0)
                {
                    result.RealPart = System.Math.Log(a.ImaginePart);
                    result.ImaginePart = System.Math.PI / 2.0;
                }
                else
                {
                    result.RealPart = System.Math.Log(-(a.ImaginePart));
                    result.ImaginePart = -System.Math.PI / 2.0;
                }
            }
            else
            {
                result.RealPart = System.Math.Log(a.Magnitude);
                result.ImaginePart = System.Math.Atan2(a.ImaginePart, a.RealPart);
            }

            return result;
        }

        /// <summary>
        /// Calculates exponent (<b>e</b> raised to the specified power) of a complex number.
        /// </summary>
        /// 
        /// <param name="a">A <see cref="calculationMethodsStructure"/> instance.</param>
        /// 
        /// <returns>Returns new <see cref="calculationMethodsStructure"/> instance containing the exponent of the specified
        /// complex number.</returns>
        /// 
        public static calculationMethodsStructure Exp(calculationMethodsStructure a)
        {
            calculationMethodsStructure result = calculationMethodsStructure.DoubleNull;
            double r = System.Math.Exp(a.RealPart);
            result.RealPart = r * System.Math.Cos(a.ImaginePart);
            result.ImaginePart = r * System.Math.Sin(a.ImaginePart);

            return result;
        }
        #endregion

        #region Public Static Complex Trigonometry

        public static calculationMethodsStructure Sin(calculationMethodsStructure a)
        {
            calculationMethodsStructure result = calculationMethodsStructure.DoubleNull;

            if (a.ImaginePart == 0.0)
            {
                result.RealPart = System.Math.Sin(a.RealPart);
                result.ImaginePart = 0.0;
            }
            else
            {
                result.RealPart = System.Math.Sin(a.RealPart) * System.Math.Cosh(a.ImaginePart);
                result.ImaginePart = System.Math.Cos(a.RealPart) * System.Math.Sinh(a.ImaginePart);
            }

            return result;
        }

        /// <summary>
        /// Calculates Cosine value of the complex number.
        /// </summary>
        /// 
        /// <param name="a">A <see cref="calculationMethodsStructure"/> instance.</param>
        /// 
        /// <returns>Returns new <see cref="calculationMethodsStructure"/> instance containing the Cosine value of the specified
        /// complex number.</returns>
        /// 
        public static calculationMethodsStructure Cos(calculationMethodsStructure a)
        {
            calculationMethodsStructure result = calculationMethodsStructure.DoubleNull;

            if (a.ImaginePart == 0.0)
            {
                result.RealPart = System.Math.Cos(a.RealPart);
                result.ImaginePart = 0.0;
            }
            else
            {
                result.RealPart = System.Math.Cos(a.RealPart) * System.Math.Cosh(a.ImaginePart);
                result.ImaginePart = -System.Math.Sin(a.RealPart) * System.Math.Sinh(a.ImaginePart);
            }

            return result;
        }

        /// <summary>
        /// Calculates Tangent value of the complex number.
        /// </summary>
        /// 
        /// <param name="a">A <see cref="calculationMethodsStructure"/> instance.</param>
        /// 
        /// <returns>Returns new <see cref="calculationMethodsStructure"/> instance containing the Tangent value of the specified
        /// complex number.</returns>
        /// 
        public static calculationMethodsStructure Tan(calculationMethodsStructure a)
        {
            calculationMethodsStructure result = calculationMethodsStructure.DoubleNull;

            if (a.ImaginePart == 0.0)
            {
                result.RealPart = System.Math.Tan(a.RealPart);
                result.ImaginePart = 0.0;
            }
            else
            {
                double real2 = 2 * a.RealPart;
                double imag2 = 2 * a.ImaginePart;
                double denom = System.Math.Cos(real2) + System.Math.Cosh(real2);

                result.RealPart = System.Math.Sin(real2) / denom;
                result.ImaginePart = System.Math.Sinh(imag2) / denom;
            }

            return result;
        }
        #endregion

        #region Overrides
        public override int GetHashCode()
        {
            return RealPart.GetHashCode() ^ ImaginePart.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return (obj is calculationMethodsStructure) ? (this == (calculationMethodsStructure)obj) : false;
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", RealPart, ImaginePart);
        }
        #endregion

        #region Comparison Operators
        
        public static bool operator ==(calculationMethodsStructure u, calculationMethodsStructure v)
        {
            return ((u.RealPart == v.RealPart) && (u.ImaginePart == v.ImaginePart));
        }

        public static bool operator !=(calculationMethodsStructure u, calculationMethodsStructure v)
        {
            return !(u == v);
        }
        #endregion

        #region Unary Operators
        
        public static calculationMethodsStructure operator -(calculationMethodsStructure a)
        {
            return calculationMethodsStructure.Negate(a);
        }
        #endregion

        #region Binary Operators
       
        public static calculationMethodsStructure operator +(calculationMethodsStructure a, calculationMethodsStructure b)
        {
            return calculationMethodsStructure.Add(a, b);
        }

        public static calculationMethodsStructure operator +(calculationMethodsStructure a, double s)
        {
            return calculationMethodsStructure.Add(a, s);
        }

        public static calculationMethodsStructure operator +(double s, calculationMethodsStructure a)
        {
            return calculationMethodsStructure.Add(a, s);
        }

        public static calculationMethodsStructure operator -(calculationMethodsStructure a, calculationMethodsStructure b)
        {
            return calculationMethodsStructure.Subtract(a, b);
        }

        public static calculationMethodsStructure operator -(calculationMethodsStructure a, double s)
        {
            return calculationMethodsStructure.Subtract(a, s);
        }

        public static calculationMethodsStructure operator -(double s, calculationMethodsStructure a)
        {
            return calculationMethodsStructure.Subtract(s, a);
        }

        public static calculationMethodsStructure operator *(calculationMethodsStructure a, calculationMethodsStructure b)
        {
            return calculationMethodsStructure.Multiply(a, b);
        }

        public static calculationMethodsStructure operator *(double s, calculationMethodsStructure a)
        {
            return calculationMethodsStructure.Multiply(a, s);
        }

        public static calculationMethodsStructure operator *(calculationMethodsStructure a, double s)
        {
            return calculationMethodsStructure.Multiply(a, s);
        }

        public static calculationMethodsStructure operator /(calculationMethodsStructure a, calculationMethodsStructure b)
        {
            return calculationMethodsStructure.Divide(a, b);
        }

        public static calculationMethodsStructure operator /(calculationMethodsStructure a, double s)
        {
            return calculationMethodsStructure.Divide(a, s);
        }

        public static calculationMethodsStructure operator /(double s, calculationMethodsStructure a)
        {
            return calculationMethodsStructure.Divide(s, a);
        }
        #endregion

        #region Conversion Operators
        
        public static explicit operator calculationMethodsStructure(float value)
        {
            return new calculationMethodsStructure((double)value, 0);
        }

        public static explicit operator calculationMethodsStructure(double value)
        {
            return new calculationMethodsStructure(value, 0);
        }
        #endregion

        #region ICloneable Members
        
        object ICloneable.Clone()
        {
            return new calculationMethodsStructure(this);
        }

        public calculationMethodsStructure Clone()
        {
            return new calculationMethodsStructure(this);
        }
        #endregion

        #region ISerializable Members
        
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Real", this.RealPart);
            info.AddValue("Imaginary", this.ImaginePart);
        }
        #endregion
    }
}